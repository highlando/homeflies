#!/bin/csh -f

# First include the system default:
set file=/usr/site-local/defaults/.cshrc.source
if ( -r $file ) then
  source $file
else
  set file=/usr/local/defaults/.cshrc.source
  if ( -r $file ) then
    source $file
  else
    echo Warning: Cannot access $file
  endif
endif

setenv LD_LIBRARY_PATH /usr/lib64/mpi/gcc/openmpi/lib64/

# Then add your own settings:
set prompt=$HOST':%C3>'

#setenv PATH "${PATH}:/net/ctrl_bigsys/fortran/3rdparty/local_mpi/bin"

alias sess 'echo "\033]30;\!*\007\c"'
alias fast 'cd /net/oresme.home/heiland/drocon'
alias linkmathabb 'ln /homes/numerik/heiland/work/genDocs/texFiles/mathComAbb.tex'
alias getmintex 'cp /homes/numerik/heiland/work/genDocs/texFiles/blankLat.tex .'
alias getgitig 'cp /homes/numerik/heiland/work/genDocs/texFiles/gitignore .gitignore'
alias g 'gvim --remote-silent'
alias dropb 'nohup ~/.dropbox-dist/dropboxd &'

alias linkmathenv 'ln /homes/numerik/heiland/work/genDocs/texFiles/mathEnv.tex'
alias linkmappis 'ln /homes/numerik/heiland/work/genDocs/texFiles/mappis0.vim'

set svnrep="file:///net/oresme.home/heiland/drocon/svnrep"
set svnloc="file:///net/oresme.home/heiland/svnloc"
set drobb="/home/heiland/Dropbox/Dropbox"
set papers="/home/heiland/moi/papers"

set svnserv="http://fs.math.tu-berlin.de:8000/svn/drocon/fastest"

# kde4 <-> matlab Problem. 
#setenv LD_LIBRARY_PATH /usr/lib64:/net/matlab/Matlab2009b/bin/glnxa64/:$LD_LIBRARY_PATH 
 
alias tree "find . -type d -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'"
