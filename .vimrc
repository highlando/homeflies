syntax on
colorscheme desert
filetype plugin on
filetype indent on

au filetype python colorscheme desert

set nonumber

"editing
set backspace=2
set sw=4
set lbr
set guifont=Source\ Code\ Pro\ 14
"keine menubar keine Toolbar
set guioptions=ca
"to fix corrupted scrolling
set ttyscroll=0

set scrolloff=5

"working with buffers
"allow changed buffers in the background
:set hidden

"alle subdirectories in den suchpfad
:set path+=**

"mapleader comma geht nicht mit latex suite
"let mapleader=","

"und los geht mit remaps
"clipboard copy and paste
"nmap ,y "*y
"nmap ,Y "*yy
"nmap ,p "*p
"zeige die Register
nmap ,r :registers<CR>
"quickfix windows
nmap ,c :copen<CR>
nmap ,cc :cclose<CR>
"close preview window
imap ,cp :pc<CR>
"zum vorherigen buffer
nmap ,b :b#<CR>
imap ,b <esc>:b#<CR>
"buffer loeschen
nmap ,d :bd<CR>
"shortcuts fuer den bufexplorer buf
imap ,cb <esc><Leader>be
nmap ,cb <Leader>be
"text completion
imap xx <c-x><c-n>
imap xn <c-n>
imap xo <c-x><c-o>

"cd in den ordner der aktuellen Datei
nmap ,cd :cd %:p:h<CR>

"jj wird zur escape taste
ino jj <esc>

"schnelles speichern auch aus dem insert mode heraus
imap ,w <esc>:w<CR>
nmap ,w :w<CR>

"search highlighting
:set hls
nnoremap <silent> <space> :nohl<CR>

"source and edit script files
nmap ,sl :source ~/.vim/synchomevim/latex.vim<CR>
nmap ,sel :e ~/.vim/synchomevim/latex.vim<CR>
"nmap ,sm :source ~/syncHome/vim/matlab.vim
nmap ,sv :source ~/.vimrc<CR>
nmap ,sev :e ~/.vimrc<CR>
"nmap ,sm :source ~/syncHome/vim/matlab.vim
nmap ,sm :source mappis.vim<CR>
nmap ,sem :e mappis.vim<CR>
"mark the position until text is changed
:set cpoptions+=$

" stop using the arrow keys
"map <down> i f**k <esc>
"map <up> i f**k <esc>
"imap <down> f**k <esc>
"imap <up> f**k <esc>

set grepprg=grep\ -nH\ $*

"====set foldlevel=99
set nofoldenable

"LaTeX suite
"see ~/.vim/ftplugin/tex_latexSuite.vim
"and ~/syncHome/vim.tex

"matlab
"source $VIMRUNTIME/macros/matchit.vim
"map <f4> :!matlab -nodesktop -nosplash < "%" <CR>

"mail
au BufRead *mail* set ft=mail

" Maps to make handling windows a bit easier
noremap <silent> ,h :wincmd h<CR>
noremap <silent> ,j :wincmd j<CR>
noremap <silent> ,k :wincmd k<CR>
noremap <silent> ,l :wincmd l<CR>

"swap items in a list to the left (gl) and to the right
nnoremap <silent> gl "_yiw:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o>/\w\+\_W\+<CR><c-l>
nnoremap <silent> gh "_yiw?\w\+\_W\+\%#<CR>:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o><c-l>

"list function definitions in quickfix window
command! -nargs=1 GJ vimgrep <q-args> % | copen
command! -nargs=1 GGJ vimgrep <q-args> ./* | copen
call pathogen#infect()
