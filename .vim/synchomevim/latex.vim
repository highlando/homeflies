":colorscheme siennaLat
:colorscheme desert
nmap ,sm ,cd :source mappis.vim<CR>
nmap ,sem ,cd :e mappis.vim<CR>
nmap ,som :e ~/work/genDocs/texFiles/mappis0.vim<CR>


"die mappings starten mit ,
"zur Wiederholung werden manche ins register m geschrieben
",, wiederholt den letzten gemapten und aufgezeichneten Befehl
"nmap ,, @m<CR> 

",f fuegt ein frame ein
nmap ,f o\frame{\frametitle{<++>}<++>}kk
imap ,f <esc>o\frame{\frametitle{<++>}<++>}kk

"move the ,, to the \ key
:imap ,, \
:imap ii $

",e macht aus dem eps unter dem cursor ein pdf
nmap ,e F{l"zyf.f.lcwpdf<esc>:cd %:p:h:exe '!epstopdf <c-R>zeps'<CR><CR>

",ll speichern und compilieren auch aus dem insert mode (,cc schliesst das
"quickfix window) 
nmap ,ll :w<CR>\ll,cc
imap ,ll <esc>:w<CR>\ll,cc

",lb bibtex
nmap ,lb ,cd :!bibtex %:r <CR>

",lmv view Pdf auf dem Mac
nmap ,lvm :!open %:r.pdf <CR>

",lc Verzeichnis sauber
nmap ,lc :!mkdir latOut<CR>:!mv %:r.* latOut/<CR>:!cp latOut/% .<CR>:!cp latOut/%:r.pdf .<CR>:!cp latOut/%:r.bib .<CR>

",ii /item in aktueller Zeile
",io /item in naechster Zeile
nmap ,ii I\item<space><esc> :let @m="I\\item \e"<CR>
imap ,ii \item<space>a
nmap ,io o\item<space>
imap ,io <esc>o\item<space>

",ic fuegt eine columns umgebung ein
nmap ,ic o<F5>columns<CR><esc>:let @c="\\column\{.5\\textwidth\}"<CR>"cpo<esc>"cp==k0f5

",l{e,s} fuegt ein label zu einer {equation,section} 
imap ,le \label{eq:
imap ,ls \label{sec:
",r{e,s} fuegt eine referenz zu einer {equation,section} 
imap ,re \eqref{eq<F9>
imap ,rs \ref{sec<F9>

",i1 fuegt ^{-1} ein
imap ,i1 ^{-1}

"Umlaute ,a ,o ,u ,ss
imap ,a \"a
imap ,o \"o
imap ,u \"u
imap ,s {\ss}
",ip ,iP inkrementiert die erste Zahl der Zeile um 1 bzw 10
"funzt insbesondere fuer Zahlen der Form .xx
"funzt auch fuer eine visual selection
nmap ,ip :s/\d\+/\=(submatch(0)+1)/<CR> :let @m=":&"<CR>
nmap ,iP :s/\d\+/\=(submatch(0)+10)/<CR> :let @m=":&"<CR>
vmap ,ip :s/\d\+/\=(submatch(0)+1)/<CR> :let @m="gv:&"<CR>
vmap ,iP :s/\d\+/\=(submatch(0)+10)/<CR> :let @m="gv:&"<CR>


",im ,iM dekrementiert die erste zahl um 1 bzw 10
nmap ,im :s/\d\+/\=(submatch(0)-1)/<cr> :let @m=":&"<CR>
nmap ,iM :s/\d\+/\=(submatch(0)-10)/<cr> :let @m=":&"<CR>
vmap ,im :s/\d\+/\=(submatch(0)-1)/<cr> :let @m="gv:&"<CR>
vmap ,iM :s/\d\+/\=(submatch(0)-10)/<cr> :let @m="gv:&"<CR>


:function! BMat(lines,rows)
:let i = 2
:put='\begin{bmatrix}'
:call InsLine(a:rows)
:while i <= a:lines
:put='\\'
:call InsLine(a:rows)
:let i = i+1
:endwhile
:put='\end{bmatrix}'
/bmatrix
:normal 2N0
:endfunction

:function! InsLine(rows)
:put='<++>'
:let j=2
:while j <= a:rows
:put='&<++>'
:normal kJ
:let j = j+1
:endwhile
:endfunction
"nur fuer zBericht, vAntrag
nmap ,lm :w<CR> :buffer dissm.tex<CR> ,ll
imap ,lm <esc>:w<CR> :buffer dissm.tex<CR> ,ll
"die mappings starten mit ,
"zur Wiederholung werden manche ins register m geschrieben
",, wiederholt den letzten gemapten und aufgezeichneten Befehl
",f fuegt ein frame ein
",e macht aus dem eps unter dem cursor ein pdf
",ll speichern und compilieren auch aus dem insert mode (,cc schliesst das
"quickfix window) 
",lmv view Pdf auf dem Mac
",lc Verzeichnis sauber
",ii /item in aktueller Zeile
",io /item in naechster Zeile
",ic fuegt eine columns umgebung ein
"Umlaute ,a ,o ,u
",ip ,iP inkrementiert die erste Zahl der Zeile um 1 bzw 10
"funzt insbesondere fuer Zahlen der Form .xx
"funzt auch fuer eine visual selection
",im ,im dekrementiert die erste zahl um 1 bzw 10
"
"copy and enclose vis selection in \index{ }
vmap id yPgv:call VEnclose('\index{','}','xxx','xxx')<CR>
