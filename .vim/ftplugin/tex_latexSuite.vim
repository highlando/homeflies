" LaTeX filetype
"	  Language: LaTeX (ft=tex)
"	Maintainer: Srinath Avadhanula
"		 Email: srinath@fastmail.fm
"


setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2

if !exists('s:initLatexSuite')
	let s:initLatexSuite = 1
	exec 'so '.fnameescape(expand('<sfile>:p:h').'/latex-suite/main.vim')

	silent! do LatexSuite User LatexSuiteInitPost
endif

silent! do LatexSuite User LatexSuiteFileType

let g:tex_flavor='latex'
let g:Tex_DefaultTargetFormat='pdf'
:TTarget pdf
let g:Tex_UseMakefile=0
let g:Tex_GotoError=1
let g:Tex_ViewRule_pdf = 'okular'
let g:Tex_ViewRule_dvi = 'okular'
let g:Tex_ViewRule_ps  = 'okular'
let g:Tex_CompileRule_pdf = 'pdflatex -synctex=1 -interaction=nonstopmode $*'
let g:Tex_CompileRule_dvi = 'latex --interaction=nonstopmod -src-special $*'  

function! SyncTexForward()
   let execstr = "silent !okular --unique %:p:r.pdf\\#src:".line(".")."%:p &"
   exec execstr
endfunction
nmap ,F :call SyncTexForward()<CR>
